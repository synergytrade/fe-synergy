FROM nginxinc/nginx-unprivileged:latest

EXPOSE 80

COPY dist/trader-ui/* /usr/share/nginx/html/
