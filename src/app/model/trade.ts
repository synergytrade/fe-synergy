import { Stocks } from "./stocks";

export class Trade {
  currentPosition: number;
  // exitProfitLoss: number;
  // longPeriod: number;
  // shortPeriod: number;
  profit: number;
  // size: number;
  // stock: Stocks;
  tradeId: number;

  constructor(public shortPeriod: number,
              public longPeriod: number,
              public exitProfitLoss: number,
              public size: number,
              public stock : Stocks,
              ){}
}
