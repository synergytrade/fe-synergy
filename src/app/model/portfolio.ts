export class Portfolio {
    currentPosition: number;
    exitProfitLoss: number;
    id: number;
    ticker: string;
    profit: number;
    size: number;
  }
  