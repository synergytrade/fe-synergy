import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TradeListComponent } from './trade-list/trade-list.component';
import { TradeService } from './service/trade.service';
import {FormsModule} from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { LineChartComponent } from './line-chart/line-chart.component';
import { Routes, RouterModule } from '@angular/router';
import { LineChartService } from './service/line-chart.service';
import { StatsComponent } from './stats/stats.component';
import { StatsService } from './service/stats.service';

const routes: Routes = [
  {path: 'line-chart', component: LineChartComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    TradeListComponent,
    LineChartComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [LineChartService, StatsService],
  bootstrap: [AppComponent]
})
export class AppModule { }


