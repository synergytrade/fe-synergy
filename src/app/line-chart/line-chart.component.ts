import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color } from 'ng2-charts';
import { LineChartService } from '../service/line-chart.service';


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})

export class LineChartComponent implements OnInit {

  // public lineChartData: ChartDataSets[];

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Stock Data'},
    { data: [69, 59, 90, 61, 56, 57, 40], label: 'Stock Data2'}
  ];

  // public lineChartLabels: String[];

  public lineChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
  public lineChartOptions: (ChartOptions & { responsive: true }) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

shortAvgs: any[];
longAvgs: any[];
timeRecords: any[];
id: number;
currentStrats: any[];

  constructor(
    protected lineChartService: LineChartService) { }

  ngOnInit() {
    // this.lineChartService.findForGraph(id).subscribe(
    //   res => {
    //     this.shortAvgs = res.map(value => value.avgShort);
    //     this.longAvgs = res.map(value => value.avgLong);
    //     this.timeRecords = res.map(value => value.currentTime);

    //     this.lineChartData = [{data: this.shortAvgs, label: 'Short Avg'},
    //                           {data: this.longAvgs, label: 'Long Avg'}];

    //    this.lineChartLabels = this.timeRecords;
    //     console.log("shortAvgs line-chart.component.ts", this.shortAvgs);
    //     console.log("longAvgs line-chart.component.ts", this.longAvgs);
    //     console.log("timeRecords line-chart.component.ts", this.lineChartLabels);
    //   },
    //   err => {
    //     console.log("error for line chart component")
    //   }
    // )
  }

}
