import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradeListComponent } from './trade-list/trade-list.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { StatsComponent } from './stats/stats.component';
 
const routes: Routes = [
  { path: 'trades', component: TradeListComponent },
  {path: 'line-chart', component: LineChartComponent},
  {path: 'stats', component: StatsComponent}
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
