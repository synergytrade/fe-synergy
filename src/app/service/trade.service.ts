import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trade } from '../model/trade';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  private newTradeSource = new Subject<Trade>();
  private tradeUrl: string;
  private stratUrl: string;
  newTradeObservable = this.newTradeSource.asObservable();

  constructor(private httpClient: HttpClient) {
    this.tradeUrl = environment.rest_host + '/trades';
    this.stratUrl = environment.rest_host + '/movingaverage/current';
  }

  newTrade(trade: Trade) {
    this.newTradeSource.next(trade);
  }
 
  public findAll(): Observable<any> {
    console.log('this.http.get<any>(this.tradeUrl) :', this.httpClient.get<Trade[]>(this.tradeUrl));
    return this.httpClient.get<any>(this.tradeUrl);
  }

  postTrade(trade: Trade): Observable<Trade> {
    console.log('posting trade: ');
    console.log(trade);
    return this.httpClient.post<Trade>(
              environment.rest_host + '/movingaverage',
              trade,
              {headers : new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  stopTrade(tradeID: number): Observable<string>{
    console.log("stopping strategy");
    return this.httpClient.put<string>(
        environment.rest_host + '/movingaverage/stop/' + tradeID,
        tradeID);
    }

  currentStrat(): Observable<any>{
    console.log('getting current strat');
    return this.httpClient.get<any>(this.stratUrl);
  }
}
