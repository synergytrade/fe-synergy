import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Portfolio } from '../model/portfolio';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  private portfolioUrl: string;

  constructor(private http: HttpClient) {
    this.portfolioUrl = environment.rest_host + '/trades';
  }
 
  public findAll(): Observable<any> {
    console.log('this.http.get<any>(this.portfolioUrl) :', this.http.get<any>(this.portfolioUrl));
    return this.http.get<any>(this.portfolioUrl);
  }
}
