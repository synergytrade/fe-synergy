import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stocks } from '../model/stocks';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StocksService {
  private stocksUrl: string;

  constructor(private http: HttpClient) {
    this.stocksUrl = environment.rest_host + '/stocks';

   }

   public findAll(): Observable<any>{
    console.log('this.http.get<any>(this.stocksUrl) :', this.http.get<any>(this.stocksUrl));
    return this.http.get<any>(this.stocksUrl);
  }
}
