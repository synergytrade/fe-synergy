import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class LineChartService {
  private graphUrl: string;

  constructor(private http: HttpClient) {
    this.graphUrl = environment.rest_host + '/graphs/';
   }

   public findForGraph(id: number): Observable<any>{
    console.log("findForGraph id", id);
    console.log("line-chat service:");
    console.log('this.http.get<any>(this.graphUrl) :', this.http.get<any>(this.graphUrl));
    return this.http.get<any>(this.graphUrl + id);
    }
}
