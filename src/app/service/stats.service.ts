import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Stats } from '../model/stats';

@Injectable({
  providedIn: 'root'
})
export class StatsService {
  private profitUrl: string;
  private noOfTradeUrl: string;

  constructor(private httpClient: HttpClient) {
    this.profitUrl = environment.rest_host + '/Statistics/ProfLoss';
    this.noOfTradeUrl = environment.rest_host + '/Statistics/TradesExicuted';
  }

  public findAll(): Observable<number>{
    console.log('this.http.get<number>(this.profitUrl) :', this.httpClient.get<number>(this.profitUrl));
    return this.httpClient.get<number>(this.profitUrl);
  }

  public findnoOfTrade(): Observable<number>{
    console.log('this.http.get<number>(this.noOfTrade) :', this.httpClient.get<number>(this.noOfTradeUrl));
    return this.httpClient.get<number>(this.noOfTradeUrl);
  }
}
