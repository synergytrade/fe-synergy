import { Component, OnInit } from '@angular/core';
import { Stats } from '../model/stats';
import { StatsService } from '../service/stats.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  profit: number;
  noOfTrade: number;

  constructor(private statsService: StatsService) { }

  ngOnInit() {
    this.updateProfit();
    this.updatenoOfTrades();
  }

  private updateProfit(){
    this.statsService.findAll().subscribe(data => {
        this.profit = data;
        console.log("profit: ", this.profit);
    } );
  }

  private updatenoOfTrades(){
    this.statsService.findnoOfTrade().subscribe(data => {
      this.noOfTrade = data;

    });
  }

}
