import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stocks } from '../app/model/stocks';
import { Trade } from '../app/model/trade';
import { Portfolio } from '../app/model/portfolio';
import { LineChart } from '../app/model/lineChart';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { StocksService } from './service/stocks.service';
import { PortfolioService } from './service/portfolio.service';
import { LineChartService } from './service/line-chart.service';
import { TradeService } from './service/trade.service';
import { StatsService } from './service/stats.service';
import { Chart, ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'ngchart';
  
  stocks: Stocks[];
  portfolios: Portfolio[];
  lineChartData: LineChart[];
  CurrentTicker: string;
  CurrentID: number;
  trade: Trade;
  tradeID: number;
  shortPeriod: number;
  longPeriod: number;
  exitProfitLoss: number;
  size: number;
  stock: Stocks;
  ticker: string;
  id: number;
  CurrentProfit: number;
  profit: number;
  currentStrats: Trade[];

  constructor(private stocksService: StocksService, private portfolioService: PortfolioService, private tradeService: TradeService, private lineChartService: LineChartService){}


  selectTicker(ticker, id){
    this.CurrentTicker = ticker;
    this.CurrentID = id;
    console.log('setting current id: ' + this.CurrentID);
  }

  profitStat(){
    
    this.CurrentProfit = this.profit;
  }

  chart(){
    console.log("making chart");
    new Chart(<HTMLCanvasElement>(document.getElementById("line-chart")), {
        type: 'line',
        data: {
          labels: ['1500','1600','1700','1750','1800','1850','1900','1950','1999','2050'],
          datasets: [{
            data: [86,114,106,106,107,111,133,221,783,2478],
            label: "A",
            borderColor: "#3e95cd",
            fill: false
          },{
            data: [282,350,411,502,635,809,947,1402,3700,5267],
            label: "B",
            borderColor: "#8e5ea2",
            fill: false
            }
          ]
        },
        options:{
          title: {
            display: true,
            text: 'sample data'
          }

        }

    });

    console.log("made chart");
  }

  postTrade(){
    console.log("postTrade entered");
   this.shortPeriod = Number((<HTMLInputElement>document.getElementById('inputValidation1')).value);
   this.longPeriod = Number((<HTMLInputElement>document.getElementById('inputValidation2')).value);
   this.exitProfitLoss = Number((<HTMLInputElement>document.getElementById('inputValidation3')).value);
   this.size = Number((<HTMLInputElement>document.getElementById('inputValidation4')).value);
   this.ticker = String(this.CurrentTicker);

    console.log("this.stock", this.stock);
    this.tradeService.postTrade(new Trade(this.shortPeriod, this.longPeriod, this.exitProfitLoss, this.size, new Stocks(this.CurrentID, this.CurrentTicker))).subscribe(
      newTrade => {
        console.log('Trade was created: ' + newTrade);
        this.tradeService.newTrade(newTrade);
      },
      error => {
        console.log('Failed to save trade');
        console.log(error)
        alert('Unable to save trade: ' + error.message);
      }
    )
    console.log("shortPeriods",this.shortPeriod);
  }

  indexToRemove: number;

  stopTrade(tradeID: number){
    console.log("stopTrade in app.component");
    console.log("this.trade", tradeID);
    this.tradeService.stopTrade(tradeID).subscribe(
      stopTrade => {
        console.log('Index: ', this.indexToRemove);
        this.currentStrats.splice(this.indexToRemove, 1);
        console.log("subscribe stop trade");
      },
      error => {
        console.log("stop trade error");
      }
    );
    console.log("stopTrade in app.component");
    console.log("trade stopped:", tradeID);
  }

  findForGraph(id) {
    var id2: any;
    id2 = document.getElementById("dropdownGraph");
    var value = id2.options[id2.selectedIndex].value;

    console.log("findForGraph");
    console.log('findForGraph(' + id2 + ')');
    
    this.lineChartService.findForGraph(id).subscribe(
      lineChartDataArray => {
        
        console.log("lineChartDataArray true");
        this.lineChartData = lineChartDataArray;
        console.log("lineChartDataArray:", lineChartDataArray);
        console.log("this.lineChartData:", this.lineChartData);
        console.log("stock ids", this.id);
      },
      error => {
        console.log('Unable to retrieve chart data');
      }
    );
  }
  ngOnInit() {
    this.stocksService.findAll().subscribe(
      stocksArray => {
        console.log("stocksArray true");
        this.stocks = stocksArray;
        console.log("stocksArray:", stocksArray);
        console.log("this.stocks:", this.stocks);
      },
      error => {
        console.log("app.component.ts constructor error (stock)", error);
      }
    );

    this.portfolioService.findAll().subscribe(
      portfolioArray => {
        console.log("portfolioArray true");
        this.portfolios = portfolioArray;
        console.log("portfolioArray:", portfolioArray);
        console.log("this.portfolios:", this.portfolios);
      },
      error => {
        console.log("app.component.ts constructor error (portfolio)", error);
      }
    );
    
    this.tradeService.currentStrat().subscribe(
      currentStratArray => {
        console.log("currentStratArray true");
        this.currentStrats = currentStratArray;
        console.log("currentStratArray:", currentStratArray);
        console.log("this.currentStrats:", this.currentStrats);
      }
    );

    
  }

}
